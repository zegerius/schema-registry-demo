---
author: Aram & Paul
date: MMMM dd, YYYY
---

# Introduction to Schema Registries

Schema Registries are a key component of event processing systems.

---

# What is a Schema Registry?

Schema Registries act as a central hub for managing schemas.

- **Definition**: Centralized storage for schema definitions
- **Role**: Ensures data compatibility and consistency across the system

---

# Importance in Event Processing

## Data Quality

1. **Ensures Data Quality**: Validates incoming data against the schema.
2. **Compatibility Checks**: Prevents breaking changes in the data schema. **Backwards compatibility is enforced!**
3. **Schema Evolution**: Manages changes in schema over time.

## Architecture

1. **Loosely coupled**: Allows for independent development of producers and consumers.
2. **Centralized**: Provides a single source of truth for the schema.

---

# Terminology

1. **Schema**: Description of structured data
2. **Subject**: Collection of schema versions
3. **Serialization format**: How data is converted to bytes

---

# Types of Schema Serialization Formats

There are two main types of serialization formats:

1. **Avro**
2. **Protobuf**
3. **JSON Schema** (not supported by Redpanda)

---

# Avro

```json
{
  "type": "record",
  "name": "stock",
  "namespace": "stock",
  "fields": [
    { "name": "date", "type": "string" },
    { "name": "last", "type": "string" },
    { "name": "volume", "type": "string" },
    { "name": "open", "type": "string" },
    { "name": "high", "type": "string" },
    { "name": "low", "type": "string" }
  ]
}
```

---

# Protobuf

```protobuf
syntax = "proto3";

package stock;

message Stock {
  string date = 1;
  string last = 2;
  string volume = 3;
  string open = 4;
  string high = 5;
  string low = 6;
}
```

---

# Why Protobuf?

Both Avro and Protobuf are good choices for schema serialization. However, Protobuf has a few advantages:

1. **Strongly Typed**: Protobuf generates strongly typed code, which makes it easier to work with.
2. **JSON format**: Protobuf has a standard JSON representation, which makes it easier to work with.
3. **Performance**: Protobuf is extremely efficient and is faster than Avro.[1]

There are two main considerations when choosing Protobuf:

1. **Requires compilation**: Language specific code must be generated.
2. **Evolution**: Strict discipline required for schema evolution

[1] See: https://softwaremill.com/data-serialization-tools-comparison-avro-vs-protobuf

---

# Versioning Protobuf

Since `proto3`, Protobuf has no required fields, so it is easy to add new fields to a schema.

## Legal change

The following changes are backwards compatible:

1. New field appended to end
2. Field name changed
3. Field type change within the same type group[1]

```protobuf
syntax = "proto3";

package stock;

message Stock {
  bytes date = 1; // <- legal: field type changed within the same type group
  string last = 2;
  string vol = 3; // <- legal: field name changed
  string open = 4;
  string high = 5;
  string low = 6;
  string close = 7; // <- legal: new field
}
```

[1] Generally speaking not recommended, but good to know. See: https://protobuf.dev/programming-guides/encoding/#structure

---

# Versioning Protobuf

Since `proto3`, Protobuf has no required fields, so it is easy to add new fields to a schema.

## Illegal changes

The following changes are not backwards compatible:

1. New field not appended to end
2. Field moved
3. Field removed

```protobuf
syntax = "proto3";

package stock;

message Stock {
  string date = 1;
  string close = 2; // <- illegal: new field not appended to end
  string last = 3;
  string open = 4; // <- illegal: moved field
  string volume = 5;
  // string high = 6; // <- illegal: removed field
  string low = 6;
}
```

---

# Managing Protobuf Schemas

The source of truth for the schema is the `.proto` file. This file is compiled into language specific code.

We commit the `.proto` file in a `/protos` directory in the root of the service repository.

## How to deal with generated code?

The `.proto` files are committed to the repository. The generated code _can_ be committed to the repository, but only if the pipeline is setup to validate the code on every commit.

---

# Subject name strategy

When a client produces a record, the schema ID is encoded in the payload header.

The schema ID is associated with a subject and version. This can be validated by the Kafka broker.

The subject depends on the 'subject name strategy', most commonly this is just `<topic-name>-value`.
