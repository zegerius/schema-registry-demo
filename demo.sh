#!/usr/bin/env bash
set -e

function curl() {
    url="https://redpanda.infra.svc.cluster.local:8081$1"
    shift
    command curl --insecure --silent --user founda:founda "$url" "$@" | jq .
}

function rpk() {
    command rpk \
        -X brokers=redpanda-0.redpanda.infra.svc.cluster.local:9094 \
        -X tls.enabled=true \
        -X tls.insecure_skip_verify=true \
        -X registry.hosts=redpanda.infra.svc.cluster.local:8081 \
        -X registry.tls.enabled=true \
        -X registry.tls.insecure_skip_verify=true \
        -X sasl.mechanism=SCRAM-SHA-512 \
        -X user=founda -X pass=founda \
        "$@"
}

# Make sure you are connected to the cluster
status=$(telepresence status --output json | jq --raw-output .user_daemon.status)
if [ "$status" != "Connected" ]; then
    echo "It looks like telepresence is not connected!" >&2
fi

# The Schema Registry has a JSON API:
curl /schemas/types
curl /subjects
curl /subjects/organization-value/versions/latest

# The 'schema' is a string representation, comments and whitespace are not preserved
curl /subjects/organization-value/versions/latest | jq --raw-output .schema

# Create a schema from a file
rpk registry schema create stock-value --schema stock.proto
rpk registry schema list

# Enable server-side schema validation
rpk cluster config set enable_schema_id_validation compat

# Create a topic with schema validation of the value enabled
rpk topic create -c redpanda.value.schema.id.validation=true stock || true

# To enable for an existing schema:
# rpk topic alter-config stock --set redpanda.value.schema.id.validation=true

# Values produced to this topic are validated, so an unknown key is not allowed:
echo '{"x": "2024-01-16"}' | rpk topic $opts produce stock --schema-id=topic --format '%v{json}\n' || true
# unable to encode value: unable to decode the record into the given schema: proto: (line 1:2): unknown field "x"

# This works (remember: in Protobuf all fields are optional)
echo '{"date": "2024-01-16"}' | rpk topic $opts produce stock --schema-id=topic --format '%v{json}\n'
# Produced to partition 0 at offset ...

# Consume the last message
# This shows the schema registry header in the value (first ~5 bytes)
rpk topic consume stock --offset -1 --num 1

# To decode the value again:
rpk topic consume stock --offset -1 --num 1 --use-schema-registry=value --format '%v\n'
